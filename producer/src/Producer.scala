import java.net.{HttpURLConnection, URL}
import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.io.{BufferedSource, Source}

object Producer {

  def main(args: Array[String]): Unit = {
    val props: Properties = new Properties()
    props.put("bootstrap.servers", "kafka:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("acks", "all")

    val textUrl = if (args.length > 1)
      args(1)
    else
      "https://www.gutenberg.org/files/100/100-0.txt"

    val producer = new KafkaProducer[String, String](props)
    val topic = "in"

    try {
      for (word <- getResource(textUrl)) {
        val record = new ProducerRecord[String, String](topic, word, word)
        producer.send(record)
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      producer.close()
    }

  }

  private def getResource(url: String,
                          connectionTimeout: Int = 10000,
                          readTimeout: Int = 20000): List[String] = {
    val connection = (new URL(url)).openConnection.asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(connectionTimeout)
    connection.setReadTimeout(readTimeout)
    connection.setRequestMethod("GET")
    val iS = connection.getInputStream
    val source = Source.fromInputStream(iS)
    val resource = tokenizeSource(source)
    if (iS != null) iS.close()
    resource
  }

  private def tokenizeSource(source: BufferedSource): List[String] = {
    source.getLines()
      .flatMap(_.split("[ \n\b]"))
      .map(_.replaceAll("[0-9\\W]", ""))
      .map(_.toLowerCase)
      .filter(_ != "")
      .toList
  }
}


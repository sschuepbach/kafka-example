import scala.util.{Failure, Success, Try}

object Io {
  def getEnv(name: String, default: String): String = {
    Try(sys.env(name)) match {
      case Success(e) => e
      case Failure(_) => default
    }
  }
}

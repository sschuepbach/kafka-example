import sbt._

object Dependencies {
  lazy val log4jV = "2.12.0"
  lazy val kafkaVersion = "2.4.0"
  lazy val kafkaConProd = "org.apache.kafka" % "kafka-clients" % kafkaVersion
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaVersion
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" % "log4j-api-scala_2.12" % "11.0"
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV

}

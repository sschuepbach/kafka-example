import java.time.Duration
import java.util.{Properties, UUID}

import org.apache.kafka.clients.consumer.KafkaConsumer

import scala.jdk.CollectionConverters._

object Consumer extends App {

  val props: Properties = new Properties()
  props.put("group.id", "consumer")
  props.put("bootstrap.servers", "kafka:9092")
  props.put("key.deserializer",
    "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer",
    "org.apache.kafka.common.serialization.LongDeserializer")
  props.put("enable.auto.commit", "true")
  props.put("auto.commit.interval.ms", "1000")

  val consumer = new KafkaConsumer[String, Long](props)
  val topics = List("out")

  try {
    consumer.subscribe(topics.asJava)
    var highestCount: (String, Long) = (null, 0)
    while (true) {
      val records = consumer.poll(Duration.ofMillis(1000))
      for (record <- records.asScala) {
        if (record.value() > highestCount._2) {
          highestCount = (record.key(), record.value())
          println(s"${record.value()}: ${record.key()}")
        }
      }
    }
  } catch {
    case e: Exception => e.printStackTrace()
  } finally {
    consumer.close()
  }
}

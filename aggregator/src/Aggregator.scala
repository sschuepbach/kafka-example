import java.time.Duration
import java.util.Properties

import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}

object Aggregator extends App {

  import Serdes._

  val props = new Properties()
  props.put("bootstrap.servers", "kafka:9092")
  props.put("application.id", "aggregator")
  props.put("acks", "all")

  val builder = new StreamsBuilder
  val source = builder.stream[String, String]("in")
  val count = source
    .groupByKey
    .count()
    .toStream
  count.to("out")

  val topology = builder.build()

  val streams = new KafkaStreams(topology, props)

  streams.start()


  sys.ShutdownHookThread {
    streams.close(Duration.ofSeconds(10))
  }

}

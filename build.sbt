import Dependencies._

ThisBuild / organization := "io.annotat"
ThisBuild / version := "0.1"
ThisBuild / scalaVersion := "2.13.1"

lazy val commonSettings = Seq(
  Compile / scalaSource := baseDirectory.value / "src",
  Test / scalaSource := baseDirectory.value / "test",
  assemblyJarName in assembly := "app.jar"
  /*libraryDependencies ++= Seq(
    log4jApi,
    log4jCore,
    log4jScala,
    log4jSlf4j
  )*/
)

lazy val utils = project
  .settings(
    Compile / scalaSource := baseDirectory.value / "src",
    Test / scalaSource := baseDirectory.value / "test",
    name := "Utils"
  )

lazy val consumer = project
  .dependsOn(utils)
  .settings(
    commonSettings,
    name := "Consumer",
    mainClass in assembly := Some("Consumer"),
    libraryDependencies ++= Seq(
      kafkaConProd
    )
  )

lazy val producer = project
  .dependsOn(utils)
  .settings(
    commonSettings,
    name := "Producer",
    mainClass in assembly := Some("Producer"),
    libraryDependencies ++= Seq(
      kafkaConProd
    )
  )

lazy val aggregator = project
  .dependsOn(utils)
  .settings(
    commonSettings,
    name := "Aggregator",
    mainClass in assembly := Some("Aggregator"),
    assemblyMergeStrategy in assembly := {
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
    libraryDependencies ++= Seq(
      kafkaStreams
    )
  )
